import React from "react";
import { ApolloProvider } from "@apollo/client";
import { createTheme, ThemeProvider } from "@mui/material";
import ApolloClient from "./apollo-client";
import { AppBar, Toolbar, Typography, Button } from "@mui/material";
import UserForm from "./components/UserForm";
import UserList from "./components/UserList";

const theme = createTheme();

function App() {
  return (
    <ApolloProvider client={ApolloClient}>
      <ThemeProvider theme={theme}>
        <AppBar position="static">
          <Toolbar>
            <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
              User Management App
            </Typography>
          </Toolbar>
        </AppBar>
        <UserForm />
        <UserList />
      </ThemeProvider>
    </ApolloProvider>
  );
}

export default App;

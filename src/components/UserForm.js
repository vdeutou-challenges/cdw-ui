import React, { useState } from "react";
import { useMutation } from "@apollo/client";
import { TextField, Button, CircularProgress, Typography } from "@mui/material";
import { CREATE_USER } from "./mutations";

function UserForm() {
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState("");
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);
  const [successMessage, setSuccessMessage] = useState("");

  const [createUser] = useMutation(CREATE_USER);

  const handleCreateUser = async () => {
    setLoading(true);
    setError(null);

    try {
      if (!firstName || !lastName || !email) {
        setError("First Name, Last Name, and Email are required.");
        setLoading(false);
        return;
      }

      const { data } = await createUser({
        variables: { input: { firstName, lastName, email } },
      });

      const newUser = data.createUser;
      setSuccessMessage("User created successfully");
      setFirstName("");
      setLastName("");
      setEmail("");
    } catch (error) {
      setError("An error occurred while creating the user.");
    } finally {
      setLoading(false);
    }
  };
  return (
    <div>
      {successMessage && (
        <Typography variant="success">{successMessage}</Typography>
      )}
      {error && <Typography variant="error">{error}</Typography>}
      <form>
        <TextField
          label="First Name"
          variant="outlined"
          value={firstName}
          onChange={(e) => setFirstName(e.target.value)}
        />
        <TextField
          label="Last Name"
          variant="outlined"
          value={lastName}
          onChange={(e) => setLastName(e.target.value)}
        />
        <TextField
          label="Email"
          variant="outlined"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
        />
        <Button
          variant="contained"
          color="primary"
          onClick={handleCreateUser}
          disabled={loading}
        >
          {loading ? (
            <CircularProgress size={24} color="inherit" />
          ) : (
            "Create User"
          )}
        </Button>
      </form>
    </div>
  );
}

export default UserForm;

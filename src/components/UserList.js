import React from "react";
import { useQuery } from "@apollo/client";
import { Box, Button, Card, CardContent, Typography } from "@mui/material";
import { GET_USERS } from "./queries";

function UserList() {
  const { loading, error, data } = useQuery(GET_USERS);

  if (loading) return <p>Loading...</p>;
  if (error) return <p>Error: {error.message}</p>;

  return (
    <Box display="flex" flexDirection="column" alignItems="center">
      <Typography variant="h5" component="h2" gutterBottom>
        User List
      </Typography>
      {data.users.map((user) => (
        <Card key={user.id} style={{ marginBottom: "16px" }}>
          <CardContent>
            <Typography variant="h6">
              {user.firstName} {user.lastName}
            </Typography>
            <Typography variant="body2" color="textSecondary">
              {user.email}
            </Typography>
          </CardContent>
        </Card>
      ))}
      <Button variant="contained" color="primary" href="/user/create">
        Create User
      </Button>
    </Box>
  );
}

export default UserList;
